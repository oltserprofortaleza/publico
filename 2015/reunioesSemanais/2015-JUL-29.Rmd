---
title: "Ata de Reunião da OLT Fortaleza"
author: David F.
date: 29/JUL/2015
output: pdf_document
---

Presentes: David, Vital, Daniel, Ronaldo.

Ana Célia ausente por motivo de saúde.

Ações à executar:

* Ana Célia: digitar as modificações do Regimento Interno;
* Daniel: 
	+ Compilar os regimentos enviados pelas outras OLTs;
	+ Verificar com o gestor da SUPGP a lista demandada na outra reunião;
	+ Verificar com o Sindicato os treinamentos da OLT na nova sede.
